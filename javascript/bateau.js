window.onload = function(){
    const canvasWidth = 900;
    const canvasHeight = 600;
    const blockSize = 5; // en pixels
    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext('2d');
    ctx.canvas.width  = window.innerWidth;
    ctx.canvas.height = window.innerHeight;
    const widthInBlocks = canvasWidth/blockSize; 
    const heightInBlocks = canvasHeight/blockSize;
    let delay; // en millisecondes
    let snakee;
    let timeout;
    init();
    function init(){
      /*canvas.style.backgroundColor = "#ddd";*/
      document.body.appendChild(canvas);
      launch();
    }
    function launch(){
      snakee = new Snake([[6,4]], "right");

      clearTimeout(timeout);
      delay = 30;
      refreshCanvas();
    }
    function refreshCanvas(){
      snakee.advance();
    
        ctx.clearRect(0,0,canvasWidth*10, canvasHeight*10);
        
        snakee.draw();
        timeout = setTimeout(refreshCanvas, delay); 
    }
    //Fonction de dessin du bateau
    function drawShip(ctx,position){
      let x = position[0] * blockSize;
      let y = position[1] * blockSize;
      ctx.fillStyle = "#FFFFFF";
            for(let j=0;j<40;j++)
            {
              ctx.fillRect(x, y+j, blockSize, blockSize);
              ctx.fillRect(x+j, y+j, blockSize, blockSize);
              ctx.fillRect(x-j, y+j, blockSize, blockSize);
              if (j%2==0)
              {
                
                for (let k=0;k<j-2;k++)
                {
                  if (k%2==0){
                    ctx.fillStyle = "#FF1010";
                    ctx.fillRect(x+k+2, y+j, blockSize, blockSize);
                  }
                  else{
                    ctx.fillStyle = "#FFFFFF";
                  }
                  
                }
                ctx.fillStyle = "#FFFFFF";
              }
            }
            for(let i=-40;i<40;i++)
            {
              ctx.fillRect(x+i, y+40, blockSize, blockSize);
            }

            ctx.fillStyle = "#1050F0";
            for(let i=-35;i<35;i++)
            {
              ctx.fillRect(x+i, y+45, blockSize, blockSize);
            }
            ctx.fillStyle = "#FFFFFF";
            for(let i=-30;i<30;i++)
            {
              ctx.fillRect(x+i, y+50, blockSize, blockSize);
            }
            ctx.fillStyle = "#1050F0";
            for(let i=-25;i<25;i++)
            {
              ctx.fillRect(x+i, y+55, blockSize, blockSize);
            }
            ctx.fillStyle = "#FFFFFF";
            for(let i=-20;i<20;i++)
            {
              ctx.fillRect(x+i, y+60, blockSize, blockSize);
            }
            for(let i=-40;i<-20;i++)
            {
              ctx.fillRect(x+i, y+80+i, blockSize, blockSize);
            }

            for(let i=-40;i<-20;i++)
            {
              ctx.fillRect(x-i+0, y+80+i, blockSize, blockSize);
            }
      


    }

    //Fonction constructrice du serpent
    function Snake(body, direction){
      this.body = body;
      this.direction = direction;
      
      this.draw = function(){
        //ctx.save();
        ctx.fillStyle = "#ff0000";
        for(let i = 0; i < this.body.length;i++){
          if (i==0)
          {
            drawShip(ctx,this.body[i]);
            
          }
          
          
        }
        //ctx.restore();
      };
      this.advance = function(){
        const nextPosition = this.body[0].slice();
        switch(this.direction){
          case "left":
            nextPosition[0] -= 1;
            break;
          case "right":
            nextPosition[0] += 1;
            break;
          case "down":
            nextPosition[1] += 1;
            break;
          case "up":
            nextPosition[1] -= 1;
            break;
          default:
            throw("Invalid direction");
        }
        this.body.unshift(nextPosition);
        
      };
      
      this.setDirection = function(newDirection){
        let allowedDirections;
        switch(this.direction){
          case "left":
          case "right":
            allowedDirections = ["up", "down","right","left"];
            break;
          case "down":
          case "up":
            allowedDirections = ["left", "right","up", "down"];
            break;
          default:
            throw("Invalid direction");
        }
        if(allowedDirections.indexOf(newDirection) > -1){
          this.direction = newDirection;
        }
      };
    
      
    }

    //Fonction constructrice de la pomme



    //Gestion des touches du clavier
    

    const map = {}; // You could also use an array
    onkeydown = onkeyup = function(e){
      e = e || event; // to deal with IE
      map[e.keyCode] = e.type == 'keydown';
      let newDirection;
      console.log(map);
      if(map[81]){
        newDirection = "left";
      } else if(map[90]){
        newDirection = "up";
      } else if(map[68]){
        newDirection = "right";
      } else if(map[83]){
        newDirection = "down";
      } else if(map[32]){
        launch();
      }
      snakee.setDirection(newDirection);
    }

    window.addEventListener('keydown', onkeyup);
  window.addEventListener('keyup', onkeydown);
}

